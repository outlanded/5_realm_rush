﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] int playerHealth = 50;
    [SerializeField] Text healthText;
    [SerializeField] AudioClip healthLossSFX;

    private void Start()
    {
        healthText.text = playerHealth.ToString();
    }

    public void DecreaseHealth()
    {
        playerHealth -= 5;
        GetComponent<AudioSource>().PlayOneShot(healthLossSFX);
        healthText.text = playerHealth.ToString();
    }
}
