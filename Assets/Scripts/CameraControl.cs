﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    float initialHeight;

    // Start is called before the first frame update
    void Start()
    {
        initialHeight = this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        {
            CameraMovement();
        }
    }

    private void CameraMovement()
    {
        //todo refactor so the camera cannot leave the map.

        var differenceStartCurrent = Mathf.Abs(initialHeight - this.transform.position.y);

        float translation = Input.GetAxis("Vertical") * 10 * Mathf.Clamp((differenceStartCurrent / 20), 1, 100);
        float rotation = Input.GetAxis("Horizontal") * 10 * Mathf.Clamp((differenceStartCurrent / 20), 1, 100);
        float zoom = Input.GetAxis("Mouse ScrollWheel") * 1000;

        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        zoom *= Time.deltaTime;

        transform.Translate(rotation, translation, zoom);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, 0, initialHeight), transform.position.z);
    }
}
