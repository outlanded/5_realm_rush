﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] Transform objectToPan;
    [SerializeField] GameObject bullets;
    [SerializeField] float attackRange = 30;
    Quaternion startingRotation;
    public Waypoint towerWaypoint;

    Transform targetEnemy;

    void Start()
    {
        startingRotation = objectToPan.rotation;
    }

    void Update()
    {
        SetTargetEnemy();
        if (targetEnemy)
        {
            objectToPan.LookAt(targetEnemy);
            FiringSwitch();
        }
        else
        {
            objectToPan.rotation = startingRotation;
            TowerShoot(false);
        }
    }

    private void SetTargetEnemy()
    {
        var sceneEnemies = FindObjectsOfType<EnemyMover>();
        if (sceneEnemies.Length == 0) { return; }

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (EnemyMover testEnemy in sceneEnemies)
        {
            closestEnemy = GetClosestEnemy(closestEnemy, testEnemy.transform);
        }

        targetEnemy = closestEnemy;
    }

    private Transform GetClosestEnemy(Transform closestEnemy, Transform testEnemy)
    {
        var distanceToCE = Vector3.Distance(transform.position, closestEnemy.position);
        var distanceToTE = Vector3.Distance(transform.position, testEnemy.position);

        if (distanceToCE > distanceToTE)
        {
            return (testEnemy);

        } else
        {
            return (closestEnemy);
        }
    }

    private void FiringSwitch()
    {
        float distanceToEnemy = Vector3.Distance(objectToPan.position, targetEnemy.position);

        if (distanceToEnemy < attackRange)
        {
            TowerShoot(true);
        }
        else
        {
            TowerShoot(false);
        }
    }

    private void TowerShoot(bool isFiring)
    {
        var gunParticles = bullets.GetComponent<ParticleSystem>().emission;
        gunParticles.enabled = isFiring;
    }
}
