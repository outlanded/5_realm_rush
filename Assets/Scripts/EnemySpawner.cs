﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] float secondsBetweenSpawns = 5;
    [SerializeField] EnemyMover enemyToSpawn;
    [SerializeField] GameObject enemyParent;
    [SerializeField] Text enemyCount;
    [SerializeField] AudioClip spawnedEnemySFX;

    int enemiesSpawned;

    // Start is called before the first frame update
    void Start()
    {
        enemyCount.text = enemiesSpawned.ToString();
        StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            print("spawning");
            Instantiate(enemyToSpawn, this.transform);
            enemiesSpawned++;
            GetComponent<AudioSource>().PlayOneShot(spawnedEnemySFX);
            enemyCount.text = enemiesSpawned.ToString();
            yield return new WaitForSeconds(secondsBetweenSpawns);
        }
    }
}
