﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{

    [SerializeField] Tower placeableTower;
    [SerializeField][Range(1,20)] int maxTowers;
    Queue<Tower> towerQueue = new Queue<Tower>();

    int currentTowers;

    public void AddTower(Waypoint baseWaypoint)
    {
        var numTowers = towerQueue.Count;

        if (numTowers < maxTowers)
        {
            InstantiateNewTower(baseWaypoint);
        }
        else
        {
            MoveExistingTower(baseWaypoint);
        }
    }

    private void InstantiateNewTower(Waypoint baseWaypoint)
    {
        var towerSpawned = Instantiate(placeableTower, baseWaypoint.transform.position, baseWaypoint.transform.rotation);
        towerSpawned.transform.parent = GetComponent<TowerFactory>().transform;
        towerQueue.Enqueue(towerSpawned);
        towerSpawned.towerWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;
    }


    private void MoveExistingTower(Waypoint baseWaypoint)
    {
        var oldTower = towerQueue.Dequeue();
        oldTower.transform.position = baseWaypoint.transform.position;
        oldTower.towerWaypoint.isPlaceable = true;
        towerQueue.Enqueue(oldTower);
        oldTower.towerWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;
    }

}
