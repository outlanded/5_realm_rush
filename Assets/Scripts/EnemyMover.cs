﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    [SerializeField] int hits = 50;
    [SerializeField] ParticleSystem hitParticlePrefab;
    [SerializeField] GameObject deathParticlePrefab;
    [SerializeField] GameObject targetParticlePrefab;
    [SerializeField] AudioClip healthLossSFX;
    [SerializeField] AudioClip enemyDeathSFX;

    AudioSource myAudioSource;

    // Start is called before the first frame update
    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        Pathfinder pathfinder = FindObjectOfType<Pathfinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }

    void OnParticleCollision(GameObject other)
    {
        hits--;
        hitParticlePrefab.Play();
        myAudioSource.PlayOneShot(healthLossSFX);
        if (hits <= 0)
        {
            KillEnemy(deathParticlePrefab);
        }
    }

    IEnumerator FollowPath(List<Waypoint> path)
    {
        foreach (Waypoint waypoint in path)
        {
            while(transform.position != waypoint.transform.position)
            transform.position = waypoint.transform.position;
            yield return new WaitForSeconds(.5f);
        }
        KillEnemy(targetParticlePrefab);
    }

    private void KillEnemy(GameObject particles)
    {
        var playerHealthObj = FindObjectOfType<PlayerHealth>();
        var mainCamera = FindObjectOfType<Camera>();

        GameObject fx = Instantiate(particles, transform.position, Quaternion.identity);
        playerHealthObj.DecreaseHealth();
        AudioSource.PlayClipAtPoint(enemyDeathSFX, mainCamera.transform.position);
        Destroy(gameObject);
    }
}
